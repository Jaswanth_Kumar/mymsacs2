﻿using mymsacs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Xunit;

namespace mymsacs.Tests.Models
{
    public class DegreeTest
    {
        [Fact]
        public void CreatesDegreeWithGivenInformation()
        {
            // Arrange
            var model = new Degree
            {
                DegreeId = 841,
                DegreeName = "MSACS",
                DegreeAbbrev = "ACS"
            };
            var validationResults = new List<ValidationResult>();

            // Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.True(success);
        }

        [Fact]
        public void degreetest1()
        {
            // Arrange
            var model = new Degree
            {
                DegreeId = 3434,
                DegreeName = "masters of science in applied computer science",
                DegreeAbbrev = "computer science"
            };
            var validationResults = new List<ValidationResult>();

            // Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.False(success);
            var failure = Assert.Single(validationResults,
                x => x.ErrorMessage == "Degree abbrevation cannot be longer than 20 characters");
            Assert.Single(failure.MemberNames, x => x == "DegreeAbbrev");
        }


    }
}
