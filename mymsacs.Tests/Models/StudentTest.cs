﻿using mymsacs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Xunit;

namespace mymsacs.Tests.Models
{
    public class StudentTest
    {
        [Fact]
        public void StudentIDTest()
        {
            var model = new Student
            {
                StudentId = 1234,
                GivenName = "manikanteswara rao ",
                FamilyName = "earla"
            };
            var validationResults = new List<ValidationResult>();

            // Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.True(success);
        }

        [Fact]
        public void StudentGivenNameTest()
        {
            var model = new Student
            {
                StudentId = 1234,
                GivenName = "manikanteswara rao shanmukha manikanta surya vamsi ravva  ",
                FamilyName = "earla"
            };
            var validationResults = new List<ValidationResult>();

            // Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.False(success);
        }
    }
}
