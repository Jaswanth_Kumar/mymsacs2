﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace  mymsacs.Models
{
    public class Degree
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DegreeId { get; set; }

        [Required]
        [Display(Name = "Degree Abbrevation")]
        [StringLength(20, ErrorMessage = "Degree abbrevation cannot be longer than 20 characters")]
        public String DegreeAbbrev { get; set; }

        [Required]
        [Display(Name = "Degree Name")]
        [StringLength(100, ErrorMessage = "Degree name cannot be longer than 100 characters")]
        public String DegreeName { get; set; }

        public ICollection<DegreeRequirement> DegreeRequirements { get; set; }
    }
}
