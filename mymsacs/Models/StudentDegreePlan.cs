﻿using mymsacs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace mymsacs.Models
{
    public class StudentDegreePlan
    {
        public StudentDegreePlan()
        {
            DegreeStatusId = 1; // default to 1
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StudentDegreePlanId { get; set; }

        public int StudentId { get; set; }

        public int DegreeId { get; set; }

        [Required]
        [Display(Name = "Plan Number (used for sorting)")]
        [StringLength(50, ErrorMessage = "Name cannot be longer than 50 characters.")]
        public int PlanNumber { get; set; }

        [Required]
        [Display(Name = "Plan Abbreviation (20 char)")]
        [StringLength(20, ErrorMessage = "Abbreviation cannot be longer than 20 characters.")]
        public string Planabbrev { get; set; }

        [Required]
        [Display(Name = "Plan Name")]
        [StringLength(50, ErrorMessage = "Name cannot be longer than 50 characters.")]
        public string PlanName { get; set; }

        public int DegreeStatusId { get; set; }

        //[Display(Name = "Includes Internship?")]
        //public Boolean IncludesInternship { get; set; }

        // Add navigation property for each related entity

        // each plan points to exactly one student
        public Student Student { get; set; }

        // each plan points to exactly one degree
        public Degree Degree { get; set; }

        // each plan points to exactly one degree status
        public DegreeStatus DegreeStatus { get; set; }

        // each plan has many terms... 
        public ICollection<PlanTerm> PlanTerms { get; set; }

    }
}



//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;
//using System.Linq;
//using System.Threading.Tasks;

//namespace  mymsacs.Models
//{
//    public class StudentDegreePlan
//    {

//        public StudentDegreePlan()
//        {
//            DegreeStatusId = 1;
//        }

//        [DatabaseGenerated(DatabaseGeneratedOption.None)]
//        public int StudentDegreePlanId { get; set; }
//        public int StudentId { get; set; }
//        public int DegreeId { get; set; }

//        [Required]
//        [Display(Name = "Plan Number")]
//        [StringLength(50, ErrorMessage = "Plan Number cannot be longer than 50 characters")]
//        public int PlanNumber { get; set; }

//        [Required]
//        [Display(Name = "Plan Abbrivation")]
//        [StringLength(50, ErrorMessage = "Plan Abbrivation cannot be longer than 50 characters")]
//        public string Planabbrev { get; set; }

//        [Required]
//        [Display(Name = "Plan Name")]
//        [StringLength(50, ErrorMessage = "Plan Name cannot be longer than 50 characters")]
//        public string PlanName { get; set; }

//        [Required]
//        [Display(Name = "Create Date")]
//        [StringLength(50, ErrorMessage = "Create Date cannot be longer than 50 characters")]
//        public string CreateDate { get; set; }

//        [Required]
//        [Display(Name = "Edit Date")]
//        [StringLength(50, ErrorMessage = "Edit Date cannot be longer than 50 characters")]
//        public string EditDate { get; set; }

//        [Required]
//        [Display(Name = "Degree Status")]
//        [StringLength(50, ErrorMessage = "Degree Status cannot be longer than 50 characters")]
//        public int DegreeStatusId { get; set; }

//        //[Required]
//        //[Display(Name = "Degree Status")]
//        //[StringLength(50, ErrorMessage = "Degree Status cannot be longer than 50 characters")]
//        //public string lkDegreeStatus { get; set; }

//        public Student Student { get; set; }
//        public Degree Degree { get; set; }

//     //   public ICollection<Student> Students { get; set; }
//      //  public ICollection<StudentDegreePlan> StudentDegreePlans { get; set; }
//    }


//}

