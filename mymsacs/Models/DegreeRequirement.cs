﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace  mymsacs.Models
{
    public class DegreeRequirement
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DegreeRequirementId { get; set; }

        [Required]
        [Display(Name = "Degree Id")]
        [StringLength(50, ErrorMessage = "Degree Id cannot be longer than 50 characters")]
        public int DegreeId { get; set; }

        [Required]
        [Display(Name = "Requirement Number")]
        [StringLength(50, ErrorMessage = "Requirement Number cannot be longer than 50 characters")]
        public int RequirementNumber { get; set; }

        [Required]
        [Display(Name = "Requirement Abbreviation")]
        [StringLength(100, ErrorMessage = "Requirement Abbreviation cannot be longer than 100 characters")]
        public string RequirementAbbrev { get; set; }

        [Required]
        [Display(Name = "Requirement Name")]
        [StringLength(100, ErrorMessage = "RequirementName cannot be longer than 100 characters")]
        public String RequirementName { get; set; }

        //[Required]
        //[Display(Name = "Degree Abbreviation")]
        //[StringLength(10, ErrorMessage = "Abbreviation cannot be longer than 50 characters")]
        //public String lkDegree { get; set; }

        public Degree Degree { get; set; }
    }
}
