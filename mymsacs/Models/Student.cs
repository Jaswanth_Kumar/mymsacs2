﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace  mymsacs.Models
{
    public class Student
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StudentId { get; set; }

        [Required]
        [Display(Name = "Given Name")]
        [StringLength(50, ErrorMessage = "GivenName cannot be longer than 50 characters")]
        public String GivenName { get; set; }

        [Required]
        [Display(Name = "Family Name")]
        [StringLength(50, ErrorMessage = "FamilyName cannot be longer than 50 characters")]
        public String FamilyName { get; set; }


        
        public ICollection<StudentDegreePlan> StudentDegreePlans { get; set; }
    }
}
