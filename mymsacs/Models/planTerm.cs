﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace mymsacs.Models
{
    public class PlanTerm
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PlanTermId { get; set; }

        public int StudentId { get; set; }
        public int DegreeId { get; set; }

        [Display(Name = "Term Number (used for sorting)")]
        public int TermNumber { get; set; }

        [Display(Name = "Term Abbreviatione.g. Fall 2017")]
        [StringLength(20, ErrorMessage = "Abbreviation cannot be longer than 20 characters.")]
        public string TermAbbrev { get; set; }

        // public int StudentDegreePlanId { get; set; }
        // Add navigation property for each related entity

        // each planterm points to exactly one student degree plan
        public Student Student { get; set; }
        public Degree Degree { get; set; }

        // each term has zerooneor many requirements... 
        public ICollection<PlanTermRequirement> PlanTermRequirements { get; set; }

        // public StudentDegreePlan StudentDegreePlan { get; set; }
    }
}
