﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace  mymsacs.Models
{
    public class PlanTermRequirement
    {
        public PlanTermRequirement()
        {
            RequirementStatusId = 1; // default to 1
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PlanTermRequirementId { get; internal set; }

        public int PlanTermId { get; internal set; }

        [Display(Name = "Term Number (used for sorting)")]
        public int TermNumber { get; internal set; }

        [Display(Name = "Requirement Number (used for sorting)")]
        public int RequirementNumber { get; internal set; }

        public int RequirementStatusId { get; internal set; }

        // Add navigation property for each related entity

        // each plan term requirement points to exactly one plan term
        public PlanTerm PlanTerm { get; set; }

        // each plan term requirement points to exactly one requirement status - have we earned credit in this term or not?
        public RequirementStatus RequirementStatus { get; set; }
    }
}


//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;
//using System.Linq;
//using System.Threading.Tasks;

//namespace  mymsacs.Models
//{
//    public class PlanTermRequirement
//    {
//        [DatabaseGenerated(DatabaseGeneratedOption.None)]
//        public int PlanTermRequirementId { get; set; }
//        public int PlanTermId { get; set; }
//        public int StudentId { get; set; }
//        public int DegreeId { get; set; }
//        public int PlanNumber { get; set; }
//        public int TermNumber { get; set; }
//        public int RequirementNumber { get; set; }


//        [Required]
//        [Display(Name = "Requirement Status")]
//        [StringLength(10, ErrorMessage = "Requirement Status cannot be longer than 10 characters")]
//        public string RequirementStatusId { get; set; }

//        public RequirementStatus RequirementStatus { get; set; }
//        public PlanTerm planTerm { get; set; }
//    }
//}
