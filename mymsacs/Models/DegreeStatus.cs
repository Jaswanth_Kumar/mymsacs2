﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace  mymsacs.Models
{
    public class DegreeStatus
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DegreeStatusId { get; internal set; }

        [Required]
        [StringLength(15, ErrorMessage = "Status cannot be longer than 15 characters")]
        public String Degreestatus { get; internal set; }
    }
}
