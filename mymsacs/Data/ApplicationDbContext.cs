﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using  mymsacs.Models;

namespace  mymsacs.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        //protected override void OnModelCreating(ModelBuilder builder)
        //{
        //    base.OnModelCreating(builder);
        //    // Customize the ASP.NET Identity model and override the defaults if needed.
        //    // For example, you can rename the ASP.NET Identity table names and more.
        //    // Add your customizations after calling base.OnModelCreating(builder);
        //}

        public DbSet<Student> Students { get; set; }
        public DbSet<Degree> Degrees { get; set; }
        public DbSet<DegreeStatus> DegreeStatuses { get; set; }
        public DbSet<RequirementStatus> RequirementStatuses { get; set; }
        public DbSet<DegreeRequirement> DegreeRequirements { get; set; }
        public DbSet<StudentDegreePlan> StudentDegreePlans { get; set; }
        public DbSet<PlanTerm> PlanTerms { get; set; }
        public DbSet<PlanTermRequirement> PlanTermRequirements { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Degree>().ToTable("Degree");
            modelBuilder.Entity<Student>().ToTable("Student");
            modelBuilder.Entity<DegreeStatus>().ToTable("DegreeStatus");
            modelBuilder.Entity<RequirementStatus>().ToTable("RequirementStatus");
            modelBuilder.Entity<DegreeRequirement>().ToTable("DegreeRequirement");
            modelBuilder.Entity<StudentDegreePlan>().ToTable("StudentDegreePlan");
            modelBuilder.Entity<PlanTerm>().ToTable("PlanTerm");
            modelBuilder.Entity<PlanTermRequirement>().ToTable("PlanTermRequirement");

        }
    }
}
