﻿using  mymsacs.Models;
using System.Linq;


namespace  mymsacs.Data
{
    public class DbInitializer
    {
        public static void Initialize(ApplicationDbContext courseSelection)
        {
            //courseSelection.Database.EnsureCreated();


            // in reverse order, delete * from all tables... 
            

            
             if (!courseSelection.Students.Any())
             {

                 var student = new Student[]
                 {
                 new Student {StudentId=1531,GivenName="Peanut",FamilyName="McNubbin"},
                 new Student {StudentId=1647,GivenName="Waffles",FamilyName="Frapenstein"},
                 new Student {StudentId=1840,GivenName="Butters",FamilyName="Bunnybill"},
                 new Student {StudentId=1537,GivenName="Bella",FamilyName="Barkley"},
                 new Student {StudentId=1675,GivenName="Max",FamilyName="Headroom"},
                 new Student {StudentId=1745,GivenName="Charlie",FamilyName="Goodchap"},
                 new Student {StudentId=1206,GivenName="Buddy",FamilyName="CoolJ"},
                 new Student {StudentId=1966,GivenName="Patch",FamilyName="Shakespaw"},
                 new Student {StudentId=1683,GivenName="Pickles",FamilyName="Pooch"},
                 new Student {StudentId=1333,GivenName="Honey",FamilyName="Dawg"},
                 new Student {StudentId=1606,GivenName="Abbie",FamilyName="Fangle"},
                 new Student {StudentId=1954,GivenName="Tess",FamilyName="Ruff"},
                 new Student {StudentId=1528,GivenName="Denise",FamilyName="Case"}

                 };

                 foreach (Student s in student)
                 {
                     courseSelection.Students.Add(s);
                     courseSelection.SaveChanges();
                 }
                 courseSelection.SaveChanges();
             }
             // base.Seed(courseSelection);



             if (!courseSelection.Degrees.Any())
             {

                 var degree = new Degree[]
                {
                 new Degree{DegreeId=1526,DegreeAbbrev="MSACS+2",DegreeName="Masters of Applied Computer Science (with 2 rereqs)"},
                 new Degree{DegreeId=1050,DegreeAbbrev="MSACS+NF",DegreeName="Masters of Applied Computer Science (with NF rereq)"},
                 new Degree{DegreeId=1547,DegreeAbbrev="MSACS+DB",DegreeName="Masters of Applied Computer Science (with DB rereq)"},
                 new Degree{DegreeId=1824,DegreeAbbrev="MSACS",DegreeName="Masters of Applied Computer Science"},
                 new Degree{DegreeId=1829,DegreeAbbrev="MSIS",DegreeName="Masters of Information Systems"},
                 new Degree{DegreeId=1850,DegreeAbbrev="MSIT",DegreeName="Masters of Information Technology"}

                };
                 foreach (Degree a in degree)
                 {
                     courseSelection.Degrees.Add(a);
                     courseSelection.SaveChanges();
                 }

                 courseSelection.SaveChanges();
             }



                 var degreeStatus = new DegreeStatus[]
                 {
                     new DegreeStatus{DegreeStatusId=1,Degreestatus="Planned"},
                     new DegreeStatus{DegreeStatusId=2,Degreestatus="In-Progress"},
                     new DegreeStatus{DegreeStatusId=3,Degreestatus="Completed"},

                 };
                 foreach (DegreeStatus s in degreeStatus)
                 {
                     courseSelection.DegreeStatuses.Add(s);
                     courseSelection.SaveChanges();
                 }
                     courseSelection.SaveChanges();

                 var requirementStatus = new RequirementStatus[]
                 {
                     new RequirementStatus{RequirementStatusId=1,Requirementstatus="Planned"},
                     new RequirementStatus{RequirementStatusId=2,Requirementstatus="In-Progress"},
                     new RequirementStatus{RequirementStatusId=3,Requirementstatus="Completed"},

                 };
                 foreach (RequirementStatus s in requirementStatus)
                 {
                     courseSelection.RequirementStatuses.Add(s);
                     courseSelection.SaveChanges();
                 }
                 courseSelection.SaveChanges();

             
            
            if (!courseSelection.DegreeRequirements.Any())
            {

                var degreeRequirements = new DegreeRequirement[] {
          new DegreeRequirement { DegreeRequirementId = 1046, DegreeId = 1526, RequirementNumber = 1, RequirementAbbrev = "356", RequirementName = "Network Fundamentals" },
          new DegreeRequirement { DegreeRequirementId = 1523, DegreeId = 1526, RequirementNumber = 2, RequirementAbbrev = "460", RequirementName = "Database Systems" },
          new DegreeRequirement { DegreeRequirementId = 1963, DegreeId = 1526, RequirementNumber = 3, RequirementAbbrev = "542", RequirementName = "Object-Oriented Programming" },
          new DegreeRequirement { DegreeRequirementId = 1521, DegreeId = 1526, RequirementNumber = 4, RequirementAbbrev = "563", RequirementName = "Developing Web Applications and Services" },
          new DegreeRequirement { DegreeRequirementId = 1383, DegreeId = 1526, RequirementNumber = 5, RequirementAbbrev = "560", RequirementName = "Advanced Topics in Database Systems" },
          new DegreeRequirement { DegreeRequirementId = 1409, DegreeId = 1526, RequirementNumber = 6, RequirementAbbrev = "555", RequirementName = "Network Security" },
          new DegreeRequirement { DegreeRequirementId = 1738, DegreeId = 1526, RequirementNumber = 7, RequirementAbbrev = "618", RequirementName = "Project Management in Business and Technology" },
          new DegreeRequirement { DegreeRequirementId = 1890, DegreeId = 1526, RequirementNumber = 8, RequirementAbbrev = "623", RequirementName = "Information Technology Management" },
          new DegreeRequirement { DegreeRequirementId = 1380, DegreeId = 1526, RequirementNumber = 9, RequirementAbbrev = "mobile", RequirementName = "Mobile Computing" },
          new DegreeRequirement { DegreeRequirementId = 1191, DegreeId = 1526, RequirementNumber = 10, RequirementAbbrev = "664", RequirementName = "Human Computer Interaction" },
          new DegreeRequirement { DegreeRequirementId = 1921, DegreeId = 1526, RequirementNumber = 11, RequirementAbbrev = "691", RequirementName = "CS Graduate Directed Project I" },
          new DegreeRequirement { DegreeRequirementId = 1755, DegreeId = 1526, RequirementNumber = 12, RequirementAbbrev = "692", RequirementName = "CS Graduate Directed Project II" },
          new DegreeRequirement { DegreeRequirementId = 1742, DegreeId = 1526, RequirementNumber = 13, RequirementAbbrev = "elective", RequirementName = "Elective" },
          new DegreeRequirement { DegreeRequirementId = 1990, DegreeId = 1526, RequirementNumber = 14, RequirementAbbrev = "comps", RequirementName = "Comprehensive Exam" },
          new DegreeRequirement { DegreeRequirementId = 1741, DegreeId = 1050, RequirementNumber = 1, RequirementAbbrev = "356", RequirementName = "Network Fundamentals" },
          new DegreeRequirement { DegreeRequirementId = 1640, DegreeId = 1050, RequirementNumber = 2, RequirementAbbrev = "542", RequirementName = "Object-Oriented Programming" },
          new DegreeRequirement { DegreeRequirementId = 1102, DegreeId = 1050, RequirementNumber = 3, RequirementAbbrev = "563", RequirementName = "Developing Web Applications and Services" },
          new DegreeRequirement { DegreeRequirementId = 1651, DegreeId = 1050, RequirementNumber = 4, RequirementAbbrev = "560", RequirementName = "Advanced Topics in Database Systems" },
          new DegreeRequirement { DegreeRequirementId = 1813, DegreeId = 1050, RequirementNumber = 5, RequirementAbbrev = "555", RequirementName = "Network Security" },
          new DegreeRequirement { DegreeRequirementId = 1501, DegreeId = 1050, RequirementNumber = 6, RequirementAbbrev = "618", RequirementName = "Project Management in Business and Technology" },
          new DegreeRequirement { DegreeRequirementId = 1312, DegreeId = 1050, RequirementNumber = 7, RequirementAbbrev = "623", RequirementName = "Information Technology Management" },
          new DegreeRequirement { DegreeRequirementId = 1361, DegreeId = 1050, RequirementNumber = 8, RequirementAbbrev = "mobile", RequirementName = "Mobile Computing" },
          new DegreeRequirement { DegreeRequirementId = 1111, DegreeId = 1050, RequirementNumber = 9, RequirementAbbrev = "664", RequirementName = "Human Computer Interaction" },
          new DegreeRequirement { DegreeRequirementId = 1946, DegreeId = 1050, RequirementNumber = 10, RequirementAbbrev = "691", RequirementName = "CS Graduate Directed Project I" },
          new DegreeRequirement { DegreeRequirementId = 1294, DegreeId = 1050, RequirementNumber = 11, RequirementAbbrev = "692", RequirementName = "CS Graduate Directed Project II" },
          new DegreeRequirement { DegreeRequirementId = 1058, DegreeId = 1050, RequirementNumber = 12, RequirementAbbrev = "elective", RequirementName = "Elective" },
          new DegreeRequirement { DegreeRequirementId = 1373, DegreeId = 1050, RequirementNumber = 13, RequirementAbbrev = "comps", RequirementName = "Comprehensive Exam" },
          new DegreeRequirement { DegreeRequirementId = 1016, DegreeId = 1547, RequirementNumber = 1, RequirementAbbrev = "460", RequirementName = "Database Systems" },
          new DegreeRequirement { DegreeRequirementId = 1017, DegreeId = 1547, RequirementNumber = 2, RequirementAbbrev = "542", RequirementName = "Object-Oriented Programming" },
          new DegreeRequirement { DegreeRequirementId = 1831, DegreeId = 1547, RequirementNumber = 3, RequirementAbbrev = "563", RequirementName = "Developing Web Applications and Services" },
          new DegreeRequirement { DegreeRequirementId = 1389, DegreeId = 1547, RequirementNumber = 4, RequirementAbbrev = "560", RequirementName = "Advanced Topics in Database Systems" },
          new DegreeRequirement { DegreeRequirementId = 1486, DegreeId = 1547, RequirementNumber = 5, RequirementAbbrev = "555", RequirementName = "Network Security" },
          new DegreeRequirement { DegreeRequirementId = 1949, DegreeId = 1547, RequirementNumber = 6, RequirementAbbrev = "618", RequirementName = "Project Management in Business and Technology" },
          new DegreeRequirement { DegreeRequirementId = 1714, DegreeId = 1547, RequirementNumber = 7, RequirementAbbrev = "623", RequirementName = "Information Technology Management" },
          new DegreeRequirement { DegreeRequirementId = 1158, DegreeId = 1547, RequirementNumber = 8, RequirementAbbrev = "mobile", RequirementName = "Mobile Computing" },
          new DegreeRequirement { DegreeRequirementId = 1195, DegreeId = 1547, RequirementNumber = 9, RequirementAbbrev = "664", RequirementName = "Human Computer Interaction" },
          new DegreeRequirement { DegreeRequirementId = 1643, DegreeId = 1547, RequirementNumber = 10, RequirementAbbrev = "691", RequirementName = "CS Graduate Directed Project I" },
          new DegreeRequirement { DegreeRequirementId = 1497, DegreeId = 1547, RequirementNumber = 11, RequirementAbbrev = "692", RequirementName = "CS Graduate Directed Project II" },
          new DegreeRequirement { DegreeRequirementId = 1884, DegreeId = 1547, RequirementNumber = 12, RequirementAbbrev = "elective", RequirementName = "Elective" },
          new DegreeRequirement { DegreeRequirementId = 1004, DegreeId = 1547, RequirementNumber = 13, RequirementAbbrev = "comps", RequirementName = "Comprehensive Exam" },
          new DegreeRequirement { DegreeRequirementId = 1797, DegreeId = 1824, RequirementNumber = 1, RequirementAbbrev = "542", RequirementName = "Object-Oriented Programming" },
          new DegreeRequirement { DegreeRequirementId = 1498, DegreeId = 1824, RequirementNumber = 2, RequirementAbbrev = "563", RequirementName = "Developing Web Applications and Services" },
          new DegreeRequirement { DegreeRequirementId = 1166, DegreeId = 1824, RequirementNumber = 3, RequirementAbbrev = "560", RequirementName = "Advanced Topics in Database Systems" },
          new DegreeRequirement { DegreeRequirementId = 1093, DegreeId = 1824, RequirementNumber = 4, RequirementAbbrev = "555", RequirementName = "Network Security" },
          new DegreeRequirement { DegreeRequirementId = 1885, DegreeId = 1824, RequirementNumber = 5, RequirementAbbrev = "618", RequirementName = "Project Management in Business and Technology" },
          new DegreeRequirement { DegreeRequirementId = 1023, DegreeId = 1824, RequirementNumber = 6, RequirementAbbrev = "623", RequirementName = "Information Technology Management" },
          new DegreeRequirement { DegreeRequirementId = 1134, DegreeId = 1824, RequirementNumber = 7, RequirementAbbrev = "mobile", RequirementName = "Mobile Computing" },
          new DegreeRequirement { DegreeRequirementId = 1006, DegreeId = 1824, RequirementNumber = 8, RequirementAbbrev = "664", RequirementName = "Human Computer Interaction" },
          new DegreeRequirement { DegreeRequirementId = 1622, DegreeId = 1824, RequirementNumber = 9, RequirementAbbrev = "691", RequirementName = "CS Graduate Directed Project I" },
          new DegreeRequirement { DegreeRequirementId = 1079, DegreeId = 1824, RequirementNumber = 10, RequirementAbbrev = "692", RequirementName = "CS Graduate Directed Project II" },
          new DegreeRequirement { DegreeRequirementId = 1918, DegreeId = 1824, RequirementNumber = 11, RequirementAbbrev = "elective", RequirementName = "Elective" },
          new DegreeRequirement { DegreeRequirementId = 1798, DegreeId = 1824, RequirementNumber = 12, RequirementAbbrev = "comps", RequirementName = "Comprehensive Exam" },
          new DegreeRequirement { DegreeRequirementId = 1875, DegreeId = 1829, RequirementNumber = 1, RequirementAbbrev = "ITM", RequirementName = "Information Technology Management" },
          new DegreeRequirement { DegreeRequirementId = 1824, DegreeId = 1829, RequirementNumber = 2, RequirementAbbrev = "ISAD", RequirementName = "Information Systems Analysis and Design" },
          new DegreeRequirement { DegreeRequirementId = 1853, DegreeId = 1829, RequirementNumber = 3, RequirementAbbrev = "OOS", RequirementName = "Developing Object-Oriented Systems with Java" },
          new DegreeRequirement { DegreeRequirementId = 1847, DegreeId = 1829, RequirementNumber = 4, RequirementAbbrev = "DDI", RequirementName = "Database Design and Implementation" },
          new DegreeRequirement { DegreeRequirementId = 1231, DegreeId = 1829, RequirementNumber = 5, RequirementAbbrev = "ENI", RequirementName = "Enterprise Networking and Internetworking" },
          new DegreeRequirement { DegreeRequirementId = 2000, DegreeId = 1829, RequirementNumber = 6, RequirementAbbrev = "SDE", RequirementName = "User Centered System Design and Evaluation" },
          new DegreeRequirement { DegreeRequirementId = 1677, DegreeId = 1829, RequirementNumber = 7, RequirementAbbrev = "INFOSEC", RequirementName = "Cybersecurity and Information Systems Security Management" },
          new DegreeRequirement { DegreeRequirementId = 1417, DegreeId = 1829, RequirementNumber = 8, RequirementAbbrev = "PISE", RequirementName = "Professionalism in the IS Environment" },
          new DegreeRequirement { DegreeRequirementId = 1927, DegreeId = 1829, RequirementNumber = 9, RequirementAbbrev = "PMIT", RequirementName = "Project Management for Business and Technology" },
          new DegreeRequirement { DegreeRequirementId = 1575, DegreeId = 1829, RequirementNumber = 10, RequirementAbbrev = "FMIT", RequirementName = "Financial Modeling and Decision Making for IT" },
          new DegreeRequirement { DegreeRequirementId = 1909, DegreeId = 1829, RequirementNumber = 11, RequirementAbbrev = "BIA", RequirementName = "Business Intelligence and Analytics" },
          new DegreeRequirement { DegreeRequirementId = 1692, DegreeId = 1829, RequirementNumber = 12, RequirementAbbrev = "CAP", RequirementName = "IS Capstone Project" },
          new DegreeRequirement { DegreeRequirementId = 1075, DegreeId = 1829, RequirementNumber = 13, RequirementAbbrev = "comps", RequirementName = "Comprehensive Exam" },
          new DegreeRequirement { DegreeRequirementId = 1530, DegreeId = 1850, RequirementNumber = 1, RequirementAbbrev = "44-515 ", RequirementName = "Effective Assessment " },
          new DegreeRequirement { DegreeRequirementId = 1257, DegreeId = 1850, RequirementNumber = 2, RequirementAbbrev = "44-582", RequirementName = "Technology Curriculum & Integration" },
          new DegreeRequirement { DegreeRequirementId = 1429, DegreeId = 1850, RequirementNumber = 3, RequirementAbbrev = "44-585", RequirementName = "Instructional Technology and the Learning Process" },
          new DegreeRequirement { DegreeRequirementId = 1900, DegreeId = 1850, RequirementNumber = 4, RequirementAbbrev = "44-614", RequirementName = "Introduction to Online Teaching/Learning" },
          new DegreeRequirement { DegreeRequirementId = 1453, DegreeId = 1850, RequirementNumber = 5, RequirementAbbrev = "44-626", RequirementName = "Multimedia Systems" },
          new DegreeRequirement { DegreeRequirementId = 1883, DegreeId = 1850, RequirementNumber = 6, RequirementAbbrev = "44-635", RequirementName = "Instructional Systems Design" },
          new DegreeRequirement { DegreeRequirementId = 1397, DegreeId = 1850, RequirementNumber = 7, RequirementAbbrev = "44-645", RequirementName = "Computers and Networks" },
          new DegreeRequirement { DegreeRequirementId = 1524, DegreeId = 1850, RequirementNumber = 8, RequirementAbbrev = "44-650   ", RequirementName = "Building Virtual Learning Environment" },
          new DegreeRequirement { DegreeRequirementId = 1318, DegreeId = 1850, RequirementNumber = 9, RequirementAbbrev = "44-656  ", RequirementName = "Current Issues in Instructional Technology" },
          new DegreeRequirement { DegreeRequirementId = 1381, DegreeId = 1850, RequirementNumber = 10, RequirementAbbrev = "44-696", RequirementName = "Graduate Directed Project" },
          new DegreeRequirement { DegreeRequirementId = 1697, DegreeId = 1850, RequirementNumber = 11, RequirementAbbrev = "Elective", RequirementName = "Elective" },
          new DegreeRequirement { DegreeRequirementId = 1319, DegreeId = 1850, RequirementNumber = 12, RequirementAbbrev = "comps", RequirementName = "IS Capstone Project" }
        };
                foreach (DegreeRequirement item in degreeRequirements)
                {
                    courseSelection.DegreeRequirements.Add(item);
                    courseSelection.SaveChanges();
                    //  LOG.LogDebug("Added {}", item);
                }
            }   courseSelection.SaveChanges();
           


            if (!courseSelection.StudentDegreePlans.Any())
            {

                var studentDegreePlans = new StudentDegreePlan[] {
          new StudentDegreePlan { StudentDegreePlanId = 1672, StudentId = 1531, DegreeId = 1526, PlanNumber = 1, Planabbrev = "Initial plan", PlanName = "My initial plan " },
          new StudentDegreePlan { StudentDegreePlanId = 1617, StudentId = 1647, DegreeId = 1050, PlanNumber = 1, Planabbrev = "Initial plan", PlanName = "My initial plan " },
          new StudentDegreePlan { StudentDegreePlanId = 1147, StudentId = 1840, DegreeId = 1547, PlanNumber = 1, Planabbrev = "Initial plan", PlanName = "My initial plan " },
          new StudentDegreePlan { StudentDegreePlanId = 1334, StudentId = 1537, DegreeId = 1824, PlanNumber = 1, Planabbrev = "Initial plan", PlanName = "My initial plan " },
          new StudentDegreePlan { StudentDegreePlanId = 1234, StudentId = 1675, DegreeId = 1829, PlanNumber = 1, Planabbrev = "Initial plan", PlanName = "My initial plan " },
          new StudentDegreePlan { StudentDegreePlanId = 1076, StudentId = 1745, DegreeId = 1850, PlanNumber = 1, Planabbrev = "Initial plan", PlanName = "My initial plan " },
          new StudentDegreePlan { StudentDegreePlanId = 1661, StudentId = 1206, DegreeId = 1526, PlanNumber = 1, Planabbrev = "Initial plan", PlanName = "My initial plan " },
          new StudentDegreePlan { StudentDegreePlanId = 1133, StudentId = 1966, DegreeId = 1050, PlanNumber = 1, Planabbrev = "Initial plan", PlanName = "My initial plan " },
          new StudentDegreePlan { StudentDegreePlanId = 1519, StudentId = 1683, DegreeId = 1547, PlanNumber = 1, Planabbrev = "Initial plan", PlanName = "My initial plan " },
          new StudentDegreePlan { StudentDegreePlanId = 1277, StudentId = 1333, DegreeId = 1824, PlanNumber = 1, Planabbrev = "Initial plan", PlanName = "My initial plan " },
          new StudentDegreePlan { StudentDegreePlanId = 1065, StudentId = 1333, DegreeId = 1829, PlanNumber = 1, Planabbrev = "Initial plan", PlanName = "My initial plan " },
          new StudentDegreePlan { StudentDegreePlanId = 1105, StudentId = 1606, DegreeId = 1829, PlanNumber = 1, Planabbrev = "Initial plan", PlanName = "My initial plan " },
          new StudentDegreePlan { StudentDegreePlanId = 1224, StudentId = 1954, DegreeId = 1850, PlanNumber = 1, Planabbrev = "Initial plan", PlanName = "My initial plan " },
          new StudentDegreePlan { StudentDegreePlanId = 1503, StudentId = 1531, DegreeId = 1526, PlanNumber = 2, Planabbrev = "internship plan", PlanName = "Plan adjusted for internship" },
          new StudentDegreePlan { StudentDegreePlanId = 1379, StudentId = 1647, DegreeId = 1050, PlanNumber = 2, Planabbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
          new StudentDegreePlan { StudentDegreePlanId = 1653, StudentId = 1840, DegreeId = 1547, PlanNumber = 2, Planabbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
          new StudentDegreePlan { StudentDegreePlanId = 1054, StudentId = 1537, DegreeId = 1824, PlanNumber = 2, Planabbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
          new StudentDegreePlan { StudentDegreePlanId = 1333, StudentId = 1675, DegreeId = 1829, PlanNumber = 2, Planabbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
          new StudentDegreePlan { StudentDegreePlanId = 1040, StudentId = 1745, DegreeId = 1850, PlanNumber = 2, Planabbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
          new StudentDegreePlan { StudentDegreePlanId = 1227, StudentId = 1206, DegreeId = 1526, PlanNumber = 2, Planabbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
          new StudentDegreePlan { StudentDegreePlanId = 1746, StudentId = 1966, DegreeId = 1050, PlanNumber = 2, Planabbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
          new StudentDegreePlan { StudentDegreePlanId = 1254, StudentId = 1683, DegreeId = 1547, PlanNumber = 2, Planabbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
          new StudentDegreePlan { StudentDegreePlanId = 1082, StudentId = 1333, DegreeId = 1824, PlanNumber = 2, Planabbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
          new StudentDegreePlan { StudentDegreePlanId = 1341, StudentId = 1333, DegreeId = 1829, PlanNumber = 2, Planabbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
          new StudentDegreePlan { StudentDegreePlanId = 1614, StudentId = 1606, DegreeId = 1829, PlanNumber = 2, Planabbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
          new StudentDegreePlan { StudentDegreePlanId = 1139, StudentId = 1954, DegreeId = 1850, PlanNumber = 2, Planabbrev = "early comps", PlanName = "early comps" }
        };
                foreach (StudentDegreePlan item in studentDegreePlans)
                {
                    courseSelection.StudentDegreePlans.Add(item);
                    courseSelection.SaveChanges();
                    // LOG.LogDebug("Added {}", item);
                }
            }
            courseSelection.SaveChanges();
             

            if (!courseSelection.PlanTerms.Any())
            {

                var planTerms = new PlanTerm[] {
                new PlanTerm{PlanTermId=4999,StudentId=1840,DegreeId=1547,TermNumber=4,TermAbbrev="Fall 2017"},
                new PlanTerm{PlanTermId=4990,StudentId=1840,DegreeId=1547,TermNumber=5,TermAbbrev="Spring 2018"},
                new PlanTerm{PlanTermId=3434,StudentId=1954,DegreeId=1850,TermNumber=1,TermAbbrev="Fall 2016"},
                new PlanTerm{PlanTermId=3933,StudentId=1954,DegreeId=1850,TermNumber=2,TermAbbrev="Spring 2017"},
                new PlanTerm{PlanTermId=4757,StudentId=1954,DegreeId=1850,TermNumber=3,TermAbbrev="Summer 2017"},
                new PlanTerm{PlanTermId=5083,StudentId=1954,DegreeId=1850,TermNumber=4,TermAbbrev="Fall 2017"},
                new PlanTerm{PlanTermId=4894,StudentId=1954,DegreeId=1850,TermNumber=1,TermAbbrev="Fall 2016"},
                new PlanTerm{PlanTermId=3520,StudentId=1954,DegreeId=1850,TermNumber=2,TermAbbrev="Spring 2017"},
                new PlanTerm{PlanTermId=4726,StudentId=1954,DegreeId=1850,TermNumber=3,TermAbbrev="Summer 2017"},
                new PlanTerm{PlanTermId=3826,StudentId=1954,DegreeId=1850,TermNumber=4,TermAbbrev="Fall 2017"},
                new PlanTerm{PlanTermId=3827,StudentId=1954,DegreeId=1850,TermNumber=5,TermAbbrev="Spring 2018"},
                new PlanTerm{PlanTermId=5031,StudentId=1966,DegreeId=1050,TermNumber=1,TermAbbrev="Fall 2016"},
                new PlanTerm{PlanTermId=4669,StudentId=1966,DegreeId=1050,TermNumber=2,TermAbbrev="Spring 2017"},
                new PlanTerm{PlanTermId=3592,StudentId=1966,DegreeId=1050,TermNumber=3,TermAbbrev="Summer 2017"},
                new PlanTerm{PlanTermId=4102,StudentId=1966,DegreeId=1050,TermNumber=4,TermAbbrev="Fall 2017"},
                new PlanTerm{PlanTermId=3819,StudentId=1966,DegreeId=1050,TermNumber=1,TermAbbrev="Fall 2016"},
                new PlanTerm{PlanTermId=5352,StudentId=1966,DegreeId=1050,TermNumber=2,TermAbbrev="Spring 2017"},
                new PlanTerm{PlanTermId=5698,StudentId=1966,DegreeId=1050,TermNumber=3,TermAbbrev="Summer 2017"},
                new PlanTerm{PlanTermId=3122,StudentId=1966,DegreeId=1050,TermNumber=4,TermAbbrev="Fall 2017"},
                new PlanTerm{PlanTermId=3222,StudentId=1966,DegreeId=1050,TermNumber=5,TermAbbrev="Spring 2018"},


        };
                foreach (PlanTerm item in planTerms)
                {
                    courseSelection.PlanTerms.Add(item);
                    courseSelection.SaveChanges();
                    //  LOG.LogDebug("Added {}", item);
                }
            } courseSelection.SaveChanges();
            



            if (!courseSelection.PlanTermRequirements.Any())
            {

                var planTermRequirements = new PlanTermRequirement[] {
          new PlanTermRequirement { PlanTermRequirementId = 30108, PlanTermId = 4999, TermNumber = 1, RequirementNumber = 1 },
          new PlanTermRequirement { PlanTermRequirementId = 36602, PlanTermId = 4999, TermNumber = 1, RequirementNumber = 2 },
          new PlanTermRequirement { PlanTermRequirementId = 48088, PlanTermId = 4999, TermNumber = 1, RequirementNumber = 3 },
          new PlanTermRequirement { PlanTermRequirementId = 26972, PlanTermId = 4999, TermNumber = 1, RequirementNumber = 4 },
          new PlanTermRequirement { PlanTermRequirementId = 20491, PlanTermId = 4999, TermNumber = 2, RequirementNumber = 5 },
          new PlanTermRequirement { PlanTermRequirementId = 11253, PlanTermId = 4999, TermNumber = 2, RequirementNumber = 6 },
          new PlanTermRequirement { PlanTermRequirementId = 34145, PlanTermId = 4999, TermNumber = 2, RequirementNumber = 7 },
          new PlanTermRequirement { PlanTermRequirementId = 38985, PlanTermId = 4999, TermNumber = 3, RequirementNumber = 8 },
          new PlanTermRequirement { PlanTermRequirementId = 25452, PlanTermId = 4999, TermNumber = 3, RequirementNumber = 9 },
          new PlanTermRequirement { PlanTermRequirementId = 47008, PlanTermId = 4999, TermNumber = 3, RequirementNumber = 11 },
          new PlanTermRequirement { PlanTermRequirementId = 11209, PlanTermId = 4999, TermNumber = 4, RequirementNumber = 12 },
          new PlanTermRequirement { PlanTermRequirementId = 17234, PlanTermId = 4999, TermNumber = 4, RequirementNumber = 13 },
          new PlanTermRequirement { PlanTermRequirementId = 41208, PlanTermId = 4999, TermNumber = 4, RequirementNumber = 14 },
          new PlanTermRequirement { PlanTermRequirementId = 26072, PlanTermId = 4999, TermNumber = 1, RequirementNumber = 1 },
          new PlanTermRequirement { PlanTermRequirementId = 30508, PlanTermId = 4990, TermNumber = 1, RequirementNumber = 2 },
          new PlanTermRequirement { PlanTermRequirementId = 24092, PlanTermId = 4990, TermNumber = 1, RequirementNumber = 3 },
          new PlanTermRequirement { PlanTermRequirementId = 13567, PlanTermId = 4990, TermNumber = 1, RequirementNumber = 4 },
          new PlanTermRequirement { PlanTermRequirementId = 40548, PlanTermId = 4990, TermNumber = 2, RequirementNumber = 5 },
          new PlanTermRequirement { PlanTermRequirementId = 25466, PlanTermId = 4990, TermNumber = 2, RequirementNumber = 6 },
          new PlanTermRequirement { PlanTermRequirementId = 23894, PlanTermId = 4990, TermNumber = 2, RequirementNumber = 7 },
          new PlanTermRequirement { PlanTermRequirementId = 24103, PlanTermId = 4990, TermNumber = 3, RequirementNumber = 0 },
          new PlanTermRequirement { PlanTermRequirementId = 26373, PlanTermId = 4990, TermNumber = 4, RequirementNumber = 6 },
          new PlanTermRequirement { PlanTermRequirementId = 15133, PlanTermId = 4990, TermNumber = 4, RequirementNumber = 8 },
          new PlanTermRequirement { PlanTermRequirementId = 30221, PlanTermId = 4990, TermNumber = 4, RequirementNumber = 9 },
          new PlanTermRequirement { PlanTermRequirementId = 22882, PlanTermId = 4990, TermNumber = 4, RequirementNumber = 11 },
          new PlanTermRequirement { PlanTermRequirementId = 34777, PlanTermId = 4990, TermNumber = 5, RequirementNumber = 10 },
          new PlanTermRequirement { PlanTermRequirementId = 14044, PlanTermId = 3434, TermNumber = 5, RequirementNumber = 12 },
          new PlanTermRequirement { PlanTermRequirementId = 39020, PlanTermId = 3434, TermNumber = 5, RequirementNumber = 13 },
          new PlanTermRequirement { PlanTermRequirementId = 26173, PlanTermId = 3434, TermNumber = 5, RequirementNumber = 14 },
          new PlanTermRequirement { PlanTermRequirementId = 16903, PlanTermId = 3434, TermNumber = 1, RequirementNumber = 1 },
          new PlanTermRequirement { PlanTermRequirementId = 34651, PlanTermId = 3434, TermNumber = 1, RequirementNumber = 2 },
          new PlanTermRequirement { PlanTermRequirementId = 39336, PlanTermId = 3434, TermNumber = 1, RequirementNumber = 3 },
          new PlanTermRequirement { PlanTermRequirementId = 31825, PlanTermId = 3434, TermNumber = 1, RequirementNumber = 4 },
          new PlanTermRequirement { PlanTermRequirementId = 28169, PlanTermId = 3933, TermNumber = 2, RequirementNumber = 5 },
          new PlanTermRequirement { PlanTermRequirementId = 46158, PlanTermId = 3933, TermNumber = 2, RequirementNumber = 6 },
          new PlanTermRequirement { PlanTermRequirementId = 40025, PlanTermId = 3933, TermNumber = 2, RequirementNumber = 7 },
          new PlanTermRequirement { PlanTermRequirementId = 21495, PlanTermId = 3933, TermNumber = 3, RequirementNumber = 8 },
          new PlanTermRequirement { PlanTermRequirementId = 44181, PlanTermId = 3933, TermNumber = 3, RequirementNumber = 9 },
          new PlanTermRequirement { PlanTermRequirementId = 37419, PlanTermId = 3933, TermNumber = 3, RequirementNumber = 11 },
          new PlanTermRequirement { PlanTermRequirementId = 27761, PlanTermId = 3933, TermNumber = 4, RequirementNumber = 12 },
          new PlanTermRequirement { PlanTermRequirementId = 16533, PlanTermId = 3933, TermNumber = 4, RequirementNumber = 13 },
          new PlanTermRequirement { PlanTermRequirementId = 16275, PlanTermId = 3933, TermNumber = 4, RequirementNumber = 14 },
          new PlanTermRequirement { PlanTermRequirementId = 35124, PlanTermId = 3933, TermNumber = 1, RequirementNumber = 1 },
          new PlanTermRequirement { PlanTermRequirementId = 14326, PlanTermId = 3933, TermNumber = 1, RequirementNumber = 2 },
          new PlanTermRequirement { PlanTermRequirementId = 46874, PlanTermId = 4757, TermNumber = 1, RequirementNumber = 3 },
          new PlanTermRequirement { PlanTermRequirementId = 34113, PlanTermId = 4757, TermNumber = 1, RequirementNumber = 4 },
          new PlanTermRequirement { PlanTermRequirementId = 25416, PlanTermId = 4757, TermNumber = 2, RequirementNumber = 5 },
          new PlanTermRequirement { PlanTermRequirementId = 35556, PlanTermId = 4757, TermNumber = 2, RequirementNumber = 6 },
          new PlanTermRequirement { PlanTermRequirementId = 36857, PlanTermId = 4757, TermNumber = 2, RequirementNumber = 7 },
          new PlanTermRequirement { PlanTermRequirementId = 25468, PlanTermId = 4757, TermNumber = 3, RequirementNumber = 0 },
          new PlanTermRequirement { PlanTermRequirementId = 35967, PlanTermId = 4757, TermNumber = 4, RequirementNumber = 6 },
          new PlanTermRequirement { PlanTermRequirementId = 25331, PlanTermId = 4757, TermNumber = 4, RequirementNumber = 8 },
          new PlanTermRequirement { PlanTermRequirementId = 37730, PlanTermId = 4757, TermNumber = 4, RequirementNumber = 9 },
          new PlanTermRequirement { PlanTermRequirementId = 24728, PlanTermId = 4757, TermNumber = 4, RequirementNumber = 11 },
          new PlanTermRequirement { PlanTermRequirementId = 29597, PlanTermId = 4757, TermNumber = 5, RequirementNumber = 10 },
          new PlanTermRequirement { PlanTermRequirementId = 25300, PlanTermId = 4757, TermNumber = 5, RequirementNumber = 12 },
          new PlanTermRequirement { PlanTermRequirementId = 15283, PlanTermId = 4757, TermNumber = 5, RequirementNumber = 13 },
          new PlanTermRequirement { PlanTermRequirementId = 16764, PlanTermId = 4757, TermNumber = 5, RequirementNumber = 14 },
          new PlanTermRequirement { PlanTermRequirementId = 43824, PlanTermId = 5083, TermNumber = 1, RequirementNumber = 1 },
          new PlanTermRequirement { PlanTermRequirementId = 42388, PlanTermId = 5083, TermNumber = 1, RequirementNumber = 2 },
          new PlanTermRequirement { PlanTermRequirementId = 36696, PlanTermId = 5083, TermNumber = 1, RequirementNumber = 3 },
          new PlanTermRequirement { PlanTermRequirementId = 42816, PlanTermId = 5083, TermNumber = 2, RequirementNumber = 4 },
          new PlanTermRequirement { PlanTermRequirementId = 45351, PlanTermId = 5083, TermNumber = 2, RequirementNumber = 5 },
          new PlanTermRequirement { PlanTermRequirementId = 27429, PlanTermId = 5083, TermNumber = 2, RequirementNumber = 6 },
          new PlanTermRequirement { PlanTermRequirementId = 32019, PlanTermId = 5083, TermNumber = 3, RequirementNumber = 7 },
          new PlanTermRequirement { PlanTermRequirementId = 28163, PlanTermId = 5083, TermNumber = 3, RequirementNumber = 8 },
          new PlanTermRequirement { PlanTermRequirementId = 44602, PlanTermId = 5083, TermNumber = 3, RequirementNumber = 10 },
          new PlanTermRequirement { PlanTermRequirementId = 18258, PlanTermId = 5083, TermNumber = 4, RequirementNumber = 9 },
          new PlanTermRequirement { PlanTermRequirementId = 13832, PlanTermId = 5083, TermNumber = 4, RequirementNumber = 11 },
          new PlanTermRequirement { PlanTermRequirementId = 32977, PlanTermId = 5083, TermNumber = 4, RequirementNumber = 12 },
          new PlanTermRequirement { PlanTermRequirementId = 12710, PlanTermId = 5083, TermNumber = 4, RequirementNumber = 13 },
          new PlanTermRequirement { PlanTermRequirementId = 36957, PlanTermId = 5083, TermNumber = 4, RequirementNumber = 14 },
          new PlanTermRequirement { PlanTermRequirementId = 37337, PlanTermId = 4894, TermNumber = 1, RequirementNumber = 1 },
          new PlanTermRequirement { PlanTermRequirementId = 49145, PlanTermId = 4894, TermNumber = 1, RequirementNumber = 2 },
          new PlanTermRequirement { PlanTermRequirementId = 35876, PlanTermId = 4894, TermNumber = 1, RequirementNumber = 3 },
          new PlanTermRequirement { PlanTermRequirementId = 48232, PlanTermId = 4894, TermNumber = 2, RequirementNumber = 4 },
          new PlanTermRequirement { PlanTermRequirementId = 39902, PlanTermId = 4894, TermNumber = 2, RequirementNumber = 5 },
          new PlanTermRequirement { PlanTermRequirementId = 24816, PlanTermId = 4894, TermNumber = 2, RequirementNumber = 6 },
          new PlanTermRequirement { PlanTermRequirementId = 44649, PlanTermId = 4894, TermNumber = 3, RequirementNumber = 0 },
          new PlanTermRequirement { PlanTermRequirementId = 26091, PlanTermId = 3520, TermNumber = 4, RequirementNumber = 7 },
          new PlanTermRequirement { PlanTermRequirementId = 23420, PlanTermId = 3520, TermNumber = 4, RequirementNumber = 8 },
          new PlanTermRequirement { PlanTermRequirementId = 28576, PlanTermId = 3520, TermNumber = 4, RequirementNumber = 10 },
          new PlanTermRequirement { PlanTermRequirementId = 49458, PlanTermId = 4726, TermNumber = 5, RequirementNumber = 9 },
          new PlanTermRequirement { PlanTermRequirementId = 15584, PlanTermId = 4726, TermNumber = 5, RequirementNumber = 11 },
          new PlanTermRequirement { PlanTermRequirementId = 22562, PlanTermId = 4726, TermNumber = 5, RequirementNumber = 12 },
          new PlanTermRequirement { PlanTermRequirementId = 13136, PlanTermId = 4726, TermNumber = 5, RequirementNumber = 13 },
          new PlanTermRequirement { PlanTermRequirementId = 39253, PlanTermId = 3826, TermNumber = 1, RequirementNumber = 1 },
          new PlanTermRequirement { PlanTermRequirementId = 30186, PlanTermId = 3826, TermNumber = 1, RequirementNumber = 2 },
          new PlanTermRequirement { PlanTermRequirementId = 28904, PlanTermId = 3826, TermNumber = 1, RequirementNumber = 3 },
          new PlanTermRequirement { PlanTermRequirementId = 40320, PlanTermId = 3827, TermNumber = 2, RequirementNumber = 4 },
          new PlanTermRequirement { PlanTermRequirementId = 33155, PlanTermId = 3827, TermNumber = 2, RequirementNumber = 5 },
          new PlanTermRequirement { PlanTermRequirementId = 41124, PlanTermId = 3826, TermNumber = 2, RequirementNumber = 6 },
          new PlanTermRequirement { PlanTermRequirementId = 49441, PlanTermId = 5031, TermNumber = 3, RequirementNumber = 7 },
          new PlanTermRequirement { PlanTermRequirementId = 35860, PlanTermId = 5031, TermNumber = 3, RequirementNumber = 8 },
          new PlanTermRequirement { PlanTermRequirementId = 13031, PlanTermId = 5031, TermNumber = 3, RequirementNumber = 10 },
          new PlanTermRequirement { PlanTermRequirementId = 35029, PlanTermId = 4669, TermNumber = 4, RequirementNumber = 9 },
          new PlanTermRequirement { PlanTermRequirementId = 33622, PlanTermId = 4669, TermNumber = 4, RequirementNumber = 11 },
          new PlanTermRequirement { PlanTermRequirementId = 18571, PlanTermId = 4669, TermNumber = 4, RequirementNumber = 12 },
          new PlanTermRequirement { PlanTermRequirementId = 29982, PlanTermId = 4669, TermNumber = 4, RequirementNumber = 13 },
          new PlanTermRequirement { PlanTermRequirementId = 48143, PlanTermId = 4669, TermNumber = 4, RequirementNumber = 14 },
          new PlanTermRequirement { PlanTermRequirementId = 21909, PlanTermId = 4669, TermNumber = 1, RequirementNumber = 1 },
          new PlanTermRequirement { PlanTermRequirementId = 22805, PlanTermId = 4669, TermNumber = 1, RequirementNumber = 2 },
          new PlanTermRequirement { PlanTermRequirementId = 24764, PlanTermId = 4669, TermNumber = 1, RequirementNumber = 3 },
          new PlanTermRequirement { PlanTermRequirementId = 22360, PlanTermId = 3592, TermNumber = 2, RequirementNumber = 4 },
          new PlanTermRequirement { PlanTermRequirementId = 24163, PlanTermId = 3592, TermNumber = 2, RequirementNumber = 5 },
          new PlanTermRequirement { PlanTermRequirementId = 43491, PlanTermId = 3592, TermNumber = 2, RequirementNumber = 6 },
          new PlanTermRequirement { PlanTermRequirementId = 26824, PlanTermId = 4102, TermNumber = 3, RequirementNumber = 0 },
          new PlanTermRequirement { PlanTermRequirementId = 11595, PlanTermId = 4102, TermNumber = 4, RequirementNumber = 7 },
          new PlanTermRequirement { PlanTermRequirementId = 11393, PlanTermId = 4102, TermNumber = 4, RequirementNumber = 8 },
          new PlanTermRequirement { PlanTermRequirementId = 29251, PlanTermId = 4102, TermNumber = 4, RequirementNumber = 10 },
          new PlanTermRequirement { PlanTermRequirementId = 33206, PlanTermId = 3819, TermNumber = 5, RequirementNumber = 9 },
          new PlanTermRequirement { PlanTermRequirementId = 39889, PlanTermId = 3819, TermNumber = 5, RequirementNumber = 11 },
          new PlanTermRequirement { PlanTermRequirementId = 38840, PlanTermId = 3819, TermNumber = 5, RequirementNumber = 12 },
          new PlanTermRequirement { PlanTermRequirementId = 23932, PlanTermId = 3819, TermNumber = 5, RequirementNumber = 13 },
          new PlanTermRequirement { PlanTermRequirementId = 10738, PlanTermId = 5352, TermNumber = 1, RequirementNumber = 1 },
          new PlanTermRequirement { PlanTermRequirementId = 22226, PlanTermId = 5698, TermNumber = 1, RequirementNumber = 2 },
          new PlanTermRequirement { PlanTermRequirementId = 10730, PlanTermId = 3122, TermNumber = 1, RequirementNumber = 3 },
          new PlanTermRequirement { PlanTermRequirementId = 34083, PlanTermId = 3222, TermNumber = 2, RequirementNumber = 4 },
           };
                foreach (PlanTermRequirement item in planTermRequirements)
                {
                    courseSelection.PlanTermRequirements.Add(item);
                    courseSelection.SaveChanges();
                }
            }   courseSelection.SaveChanges();

        }
    }
}
